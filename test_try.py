import sqlite3
import uuid


status_list = { "IN_PROGRESS": 0, "OK": 1, "ERROR": 2, "ERROR_ARCHIVE": 3}

db = "tmp.db"


def db_add_task(db, status):
    conn = sqlite3.connect(db)
    c = conn.cursor()

    u = uuid.uuid4()
    c.execute('INSERT INTO tasks (uuid, status_id) VALUES (?, ?)', (unicode(u), status))

    conn.commit()
    conn.close()


def create_db(db):
    conn = sqlite3.connect(db)
    try:
        with conn:
            conn.execute('''CREATE TABLE IF NOT EXISTS status_list (
                            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                            status TEXT
                        )''')
    except Exception as e:
        print "I have an error:"
        raise

def create_db2(db):
    conn = sqlite3.connect(db)
    try:
        with conn:
            conn.execute('''CREATE TABLE IF NOT EXISTS status_list (
                            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                            status TEXT
                        )''')
    except:
        print "I have an error:"
        return False
    return True


def main():
    try:
        create_db("/dev/null")
    except Exception as e:
        print e.message

    # if create_db2("/dev/null"):
    #     print "go ahead"

if __name__ == "__main__":
    main()