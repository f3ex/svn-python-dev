import argparse


def create_argv_parse():
    parser = argparse.ArgumentParser(description='Do svn checkout and create an archive')

    parser.add_argument('-r', action='store', dest='repo_file', required=True, help='path to a repo file')
    parser.add_argument('-w', action='store', dest='work_dir', default="/tmp", help='work dir, default is /tmp')
    parser.add_argument('-d', action='store', dest='db_name', help='sqlite db path, default ${WORK_DIR}/svn.db')
    parser.add_argument('-f', action='store_true',
                        help="Force create an archive with skipping svn checking errors")

    return parser

p = create_argv_parse()

args = p.parse_args()


print args

