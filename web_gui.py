#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import sqlite3
from urllib3 import request
from flask import g
from flask import Flask
from flask import render_template
from flask import request
import datetime


app = Flask(__name__)

DATABASE = 'svn.db'


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE, detect_types=sqlite3.PARSE_DECLTYPES)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


def get_color_by_error(status):
    if status == 'OK':
        color = 'green'
    elif status == 'ERROR' or status == 'ERROR_ARCHIVE':
        color = 'red'
    elif status == 'IN_PROGRESS':
        color = 'blue'
    else:
        color = "black"

    return color


def valid_year(year):
    if year and year.isdigit():
        year = int(year)
        if 1900 <= year <= 2020:
            return year


def valid_month(month):
    if month and month.isdigit():
        month = int(month)
        if 1 <= month <= 12:
            return month


def valid_day(day):
    if day and day.isdigit():
        day = int(day)
        if 1 <= day <= 32:
            return day


@app.route('/', methods=['GET', 'POST'])
def show_tasks():
    t_begin = query_db("""SELECT task_begin FROM tasks ORDER BY task_begin LIMIT 1""")
    t_end = query_db("""SELECT task_finish FROM tasks ORDER BY task_begin DESC LIMIT 1""")
    show_search = True
    if not t_begin[0]:
        show_search = False
    year_begin = t_begin[0][0].year
    year_end = t_end[0][0].year

    year_list = []
    if year_end == year_begin:
        year_list.append(year_begin)
    else:
        while year_begin <= year_end:
            year_list.append(year_begin)
            year_begin += 1

    print year_list

    month_list = []
    for i in range(1, 13):
        month_list.append(i)

    days_list = []
    for i in range(1, 32):
        days_list.append(i)

    if request.method == 'POST':
        print request.form
        year_from = valid_year(request.form['year_from'])
        month_from = valid_month(request.form['month_from'])
        day_from = valid_day(request.form['day_from'])
        year_to = valid_year(request.form['year_to'])
        month_to = valid_month(request.form['month_to'])
        day_to = valid_day(request.form['day_to'])

        date_from = datetime.datetime(year_from, month_from, day_from)
        date_to = datetime.datetime(year_to, month_to, day_to)


        tasks = query_db("""SELECT T.id, T.uuid, datetime(T.task_begin, 'localtime'), T.task_finish, S.status FROM tasks T
                            INNER JOIN status_list S
                            ON T.status_id=S.id
                            WHERE
                                T.task_begin >= ? AND
                                T.task_finish <= ?""",
                         (date_from, date_to))
    else:
        tasks = query_db("""SELECT T.id, T.uuid, datetime(T.task_begin, 'localtime'), T.task_finish, S.status FROM tasks T
                        INNER JOIN status_list S
                        ON T.status_id=S.id""")

    out = "<tr><th>ID</th><th>Archive name / task UUID</th><th>Task started</th><th>Task finished</th><th>Status</th></tr>"

    print tasks
    for task in tasks:
        status = task[4]
        color = get_color_by_error(status)
        out += """<tr><th>%d</th> <th><a href="/task/%s">%s</a> </th><th> %s </th><th> %s </th>
                    <th> <font color="%s">%s</font></th> </tr>\n""" \
               % (task[0], task[1], task[1], task[2], task[3].strftime("%Y-%m-%d %H:%M:%S"), color, task[4])

    print out
    return render_template('index.html', text=out, year_list=year_list, month_list=month_list, days_list=days_list,
                                        show_search=show_search)


@app.route('/task/<task_id>')
def get_task(task_id):
    q = query_db("SELECT EXISTS(SELECT 1 FROM tasks WHERE uuid =? LIMIT 1)", (task_id,))

    if not q[0][0]:
        return "Record is not found", 404

    jobs = query_db("""SELECT J.id, J.uuid, J.url, datetime(J.job_begin, 'localtime'), J.job_finish, J.error_text, S.status FROM jobs J
                    INNER JOIN status_list S
                    ON J.status_id=S.id
                    WHERE J.task_id=?""", (task_id,))
    print "%r" % jobs
    out = "<tr><th>ID</th><th>JOB UUID</th><th>SVN URL</th><th>Task started</th><th>Task finished</th><th>SVN Error</th><th>Status</th></tr>"

    for job in jobs:
        status = job[6]
        color = get_color_by_error(status)
        out += """<tr><th>%d </th><th> %s </th><th> %s </th><th> %s </th><th> %s </th><th>%s </th><th> <font color="%s">%s</font> </th></tr>\n""" \
               % (job[0], job[1], job[2], job[3], job[4].strftime("%Y-%m-%d %H:%M:%S"), job[5], color, job[6])

    print out

    return render_template('task.html', text=out)


@app.route('/job/<job_id>')
def get_job(job_id):
    return 'Job %r' % job_id
