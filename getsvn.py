#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import svn.remote
import re
import uuid
import os
import sqlite3
import sys
from datetime import datetime
import tarfile
import traceback
import argparse
import logging.handlers


"""
Добавить конфиг
Открытие БД из каталога с проектом
+ Поправить использование одинарных и двойных кавычек
+ Добавить логирование
+ Добавить текст ошибки SVN CO  в базу в jobs
+ Добавить аргументы
+ Директории клеить через os path join

https://docs.python.org/2/reference/datamodel.html
"""

# I had an error in svn module:
# UnicodeDecodeError: 'ascii' codec can't decode byte 0xd0 in position 918: ordinal not in range(128)
# http://stackoverflow.com/questions/21129020/how-to-fix-unicodedecodeerror-ascii-codec-cant-decode-byte
# encoding=utf8
reload(sys)
sys.setdefaultencoding('utf8')

status_list = { "IN_PROGRESS": 0, "OK": 1, "ERROR": 2, "ERROR_ARCHIVE": 3}
DEBUG = False
log = None


class GetSVNError(Exception):
    def __init__(self, arg):
        self.message = arg


class DBError(GetSVNError):
    pass


class ArchiveError(GetSVNError):
    pass


class CatalogError(GetSVNError):
    pass

class UserError(GetSVNError):
    pass


def parse_args():
    p = argparse.ArgumentParser(description='Do svn checkout and create an archive')

    p.add_argument('-r', action='store', dest='repo_file', required=True, help="Path to a repo file")
    p.add_argument('-w', action='store', dest='work_dir', default='/tmp', help="Work dir, default is /tmp")
    # p.add_argument('-db', action='store', dest='db_name', help="SQLite db path, default ${WORK_DIR}/svn.db")
    p.add_argument('-d', action='store_true', help="Enable debug")
    p.add_argument('-f', action='store_true', help="Force create an archive with skipping svn checking errors")

    return p.parse_args()


def check_db(db):
    from os.path import isfile, getsize

    if not isfile(db):
        return False
    if getsize(db) < 100: # SQLite database file header is 100 bytes
        return False

    with open(db, 'rb') as fd:
        header = fd.read(100)

    return header[:16] == 'SQLite format 3\x00'


def db_create(db):
    """If there isn't db file, create new DB with scheme"""

    status_db = check_db(db)

    if DEBUG:
        print "DB has status %r" % status_db

    if os.path.isfile(db):
        if not status_db:
            raise DBError("DB was corrupted.")
        else:
            return

    conn = sqlite3.connect(db)

    try:
        with conn:
            conn.execute('''CREATE TABLE IF NOT EXISTS tasks (
                            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                            uuid TEXT NOT NULL,
                            status_id INTEGER NOT NULL,
                            task_begin TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                            task_finish TIMESTAMP
                        )''')

            conn.execute('''CREATE TABLE IF NOT EXISTS status_list (
                            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                            status TEXT
                        )''')

            # (|id|uuid|url|work_id|)
            conn.execute('''CREATE TABLE IF NOT EXISTS jobs (
                            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                            uuid TEXT NOT NULL,
                            url TEXT NOT NULL,
                            status_id INTEGER NOT NULL,
                            error_text TEXT,
                            task_id INTEGER NOT NULL,
                            job_begin TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                            job_finish TIMESTAMP,
                            FOREIGN KEY(status_id) REFERENCES status_list(id),
                            FOREIGN KEY(task_id) REFERENCES tasks(id)
                        )''')

            add_status = "INSERT INTO status_list (id, status) VALUES (?, ?)"

            for key, value in status_list.iteritems():
                status = (value, key)
                conn.execute(add_status, status)

    except Exception as e:
        print "%r" % e.message
        raise DBError("DB was corrupted.")


def db_add_task(db):
    conn = sqlite3.connect(db)

    try:
        with conn:
            status = status_list['IN_PROGRESS']
            u = uuid.uuid4()
            conn.execute("INSERT INTO tasks (uuid, status_id) VALUES (?, ?)", (unicode(u), status))
            return u
    except Exception as e:
        print "%r" % e.message
        raise DBError("DB was corrupted.")


def db_task_update_status(db, u, status):

    conn = sqlite3.connect(db)
    try:
        with conn:
            # Check the record exists
            c = conn.execute("SELECT EXISTS(SELECT 1 FROM tasks WHERE uuid =? LIMIT 1)", (u,))

            if not c.fetchone()[0]:
                raise DBError("DB was corrupted.")

            conn.execute("UPDATE tasks SET status_id =?, task_finish =? WHERE uuid =? LIMIT 1",
                      (status, datetime.now(), u,))
    except Exception as e:
        print "%r" % e.message
        raise DBError("DB was corrupted.")


def db_add_job(db, job_id, task_id, url, status, error_text=''):
    conn = sqlite3.connect(db)

    try:
        with conn:
            conn.execute("INSERT INTO jobs (uuid, task_id, url, status_id, error_text ) VALUES (?, ?, ?, ?, ?)",
                            (job_id, task_id, url, status, unicode(error_text)))
    except Exception as e:
        print "%r" % e.message
        raise DBError("DB was corrupted.")


def db_job_update(db, job_id, status, error_text=''):
    conn = sqlite3.connect(db)

    try:
        with conn:

            c = conn.execute("SELECT EXISTS(SELECT 1 FROM jobs WHERE uuid =? LIMIT 1)", (job_id,))
            if not c.fetchone()[0]:
                raise DBError("DB was corrupted.")

            conn.execute("UPDATE jobs SET status_id =?, job_finish =?, error_text =? WHERE uuid =?",
                            (status, datetime.now(), unicode(error_text), job_id))
    except Exception as e:
        print "%r" % e.message
        raise DBError("DB was corrupted.")


def check_file_exists(f):
    if not (os.path.isfile(f) and os.access(f, os.R_OK)):
        raise UserError("The problem with the file")


def check_dir_exists(d):
    if not (os.path.isdir(d) and os.access(d, os.R_OK)):
        raise UserError("The problem with the dir")


def svn_checkout(repo_file, work_dir_project, db_path, u):
    transaction_error = False

    with open(repo_file, 'rU') as f:
            for url_svn in f:
                # If need deep validate url, it should be moved to an individual function
                if re.match(r'^http', url_svn):
                    url_svn = url_svn.strip()
                    print "%r ->" % url_svn,
                    unique_filename = str(uuid.uuid4())
                    work_dir_svn = os.path.join(work_dir_project, unique_filename)
                    print "%r" % work_dir_svn

                    if not os.path.exists(work_dir_svn):
                        os.makedirs(work_dir_svn)

                    r = svn.remote.RemoteClient(url_svn.strip())
                    try:
                        db_add_job(db_path, str(unique_filename), u, url_svn, status_list['IN_PROGRESS'])
                        log.info("Working with %r" % url_svn)
                        r.checkout(work_dir_svn)
                        log.info("URL %r is done" % url_svn)
                        db_job_update(db_path, str(unique_filename), status_list['OK'])
                    except svn.common.SvnException as e:
                        log.debug("The error die downloading %r" % url_svn)
                        db_job_update(db_path, str(unique_filename), status_list['ERROR'], e.message)
                        transaction_error = True
                        print "The problem due downloading a repository: (%r) %r - %r" % (type(e), e.message, e.__doc__)

    return transaction_error


def add_logger(log_level):
    log = logging.getLogger(__name__)

    log.setLevel(log_level)

    handler = logging.handlers.SysLogHandler(address="/dev/log")

    formatter = logging.Formatter("%(module)s.%(funcName)s: %(message)s")
    handler.setFormatter(formatter)

    log.addHandler(handler)

    return log


def make_archive(archive_file, work_dir_project, db_path, u):
    try:
        with tarfile.open(archive_file, "w:gz") as tar:
            tar.add(os.path.join(work_dir_project, ""), arcname=os.path.basename(os.path.join(work_dir_project, "")))
        log.info("An archive is done.")
        print "\nCaught it!\nThe path for downloading is %r" % archive_file
    except:
        db_task_update_status(db_path, u, status_list['ERROR_ARCHIVE'])
        log.critical("Error while making archive.")
        raise ArchiveError("Error with archive")


def main():
    try:
        # default vars
        db_name = "svn.db"
        work_dir = "/tmp"

        global log
        log = add_logger(logging.DEBUG)

        # parse args
        # p = create_argv_parse()
        args = parse_args()
        global DEBUG
        DEBUG = args.d
        log.debug("ARGS %r" % args)
        if DEBUG:
            print args

        force_flag = args.f

        repo_file = args.repo_file
        check_file_exists(repo_file)

        if args.work_dir:
            work_dir = args.work_dir
        log.debug("work_dir is %r" % work_dir)

        check_dir_exists(work_dir)

        # db_path = os.path.join(work_dir, db_name)
        db_path = db_name
        log.debug("DB path is %r" % db_path)

        # create DB and work with DB
        db_create(db_path)
        u_uuid = db_add_task(db_path)
        u = str(u_uuid)
        log.info("Project uuid: %r" % u)
        print "TASK UUID: %r" % u

        archive_file = '/tmp/%s.tgz' % u
        # pid = os.getpid()
        # print "PID: %r" % pid
        work_dir_project = os.path.join(work_dir, u)

        if not os.path.exists(work_dir):
            raise CatalogError("Work dir %r doesn't exist" % work_dir)

        if os.path.exists(work_dir_project):
            raise CatalogError("Work project dir %r does exist" % work_dir_project)
        else:
            os.makedirs(work_dir_project)

        transaction_error = svn_checkout(repo_file, work_dir_project, db_path, u)

        if transaction_error:
            db_task_update_status(db_path, u, status_list['ERROR'])
            log.critical("Transaction ERROR")
            if force_flag:
                log.critical("Using force to create an archive")
                make_archive(archive_file, work_dir_project, db_path, u)
            else:
                raise ArchiveError("The transaction is failed.")
        else:
            db_task_update_status(db_path, u, status_list['OK'])
            log.info("Transaction OK")
            make_archive(archive_file, work_dir_project, db_path, u)

    except DBError as e:
        print "I have an error %r. Please check the DB." % e.message
        if DEBUG:
            traceback.print_exc()
        exit(1)
    except ArchiveError as e:
        print "I have an error %r due making an archive." % e.message
        if DEBUG:
            traceback.print_exc()
        exit(2)
    except CatalogError as e:
        print "I have an error %r due creating work dirs" % e.message
        if DEBUG:
            traceback.print_exc()
        exit(3)
    except UserError as e:
        print "I have an error %r with user data" % e.message
        if DEBUG:
            traceback.print_exc()
        exit(4)
    except Exception as e:
        if DEBUG:
            traceback.print_exc()
        print "A have the biggest unresolved problem. Cannot continue: (%r) %r %r" % (type(e), e.message, e.__doc__)
        exit(10)

if __name__ == "__main__":
        main()
