# -*- coding: utf-8 -*-
from hashlib import md5
import os

BASE_PATh = '/tmp'


def get_hash(str_hased):
    m = md5()
    m.update(str_hased)
    return m.hexdigest()


class SvnLoader(object):
    def __init__(self, url_list=None):
        if url_list:
            self.job_key = get_hash('|'.join([str(i) for i in url_list]))
        else:
            self.clean_not_complete()
            # get_from db data
            #id
            #url_list
            #count_try
            #time
            pass

        #debug
        self.url_list = url_list  # by db if exist else url_list


    def clean_not_complete(self):
        for url in self.url_list:
            #get data from db loaded 100%
            if not url:
                os.rmdir(self.get_url_path(url))
                #or svn up

    @property
    def project_path(self):
        return os.path.abspath('{}/{}'.format(BASE_PATh, self.job_key))

    def get_url_path(self, url):
        return '{}/{}'.format(self.project_path, get_hash(url))

    def create_home(self):
        if not os.path.exists():
            os.mkdir(self.project_path)

    def load_link(self, link):
        ok = False

        # svn load if 'ok' ok = True

        return True if ok and self.commit_url(get_hash(link)) else False

    def commit_url(self, uid):
        # db commit current_url by uid
        pass

    def commit_project(self):
        all_link_ok = True
        for link in self.url_list:
            #cheak link download ok in files and db commit sucs
            if not link:
                all_link_ok = False
                break
        if not all_link_ok:
            print 'fail'
            return False

        print 'all ok'
        return True